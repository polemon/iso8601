/**
 * iso8601 - display ISO date and time.
 *
 * Copyright (c) 2012 - 2016, Szymon 'polemon' Bereziak <polemon@polemon.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VERSION "0.3"
#define DATE    __DATE__

#define STR_EXPAND(tok) #tok

#if defined (__WIN32__) || defined (__WINNT__)
    #define FNAME "iso8601.exe"
    #define VER_ARCH VERSION "-win32 (MinGW)"
#elif defined (__MSDOS__)
    #define FNAME "ISO8601.EXE"
    #define VER_ARCH VERSION "-msdos (DJGPP)"
#elif defined (__unix__)
    #define FNAME "iso8601"
#else
    #define FNAME "ISO8601"
#endif

#ifndef VER_ARCH
    #define VER_ARCH VERSION
#endif

#define S_DAYS(d) ((d) * 24 * 60 * 60)

// ISO week date
typedef struct iwd {
    int iw_year;
    int iw_week;
    int iw_wday;
} Iwd;

// options
static int basic     = 0;
static int sh_date   = 0;
static int sh_time   = 0;
static int sh_offset = 0;

static int weekdate = 0;
static int ordinal  = 0;

static char *optstring = "hbdtowlv";

static void usage(int help) {
    fprintf(stderr,
            "%s v%s, build: %s\n"
            "Copyright (c) 2012 Szymon 'polemon' Bereziak <polemon@polemon.org>\n"
            , FNAME, VER_ARCH, DATE);

    if(help) {
        fprintf(stderr, "\n"
                "Usage: %s [OPTION]...\n\n"
                "Displays an ISO 8601 (RFC 3339) date and time profile.\n\n"
                "Options:\n"
                "  (none)  display full ISO 8601 date and time with offset\n"
                "  -h      display the help message\n"
                "  -b      basic format with no delimiters\n"
                "  -d      display date\n"
                "  -t      display time\n"
                "  -o      display UTC offset\n"
                "  -w      use ISO week date instead of calendar date\n"
                "  -l      use ordinal date instead of calendar date\n"
                "  -v      display version and build date\n\n"
                " * Both -w and -l assume -d. Use -t and/or -o with either of those to get\n"
                "   a full timestamp.\n\n"
                " * No options assume -d, -t, and -o.\n\n"
                " * Either use -w or -l, as they're mutually exclusive. -w takes precendece.\n\n"
                "References:\n"
                "  http://enwp.org/ISO_8601\n"
                "  http://enwp.org/ISO_week_date\n"
                "  http://www.ietf.org/rfc/rfc3339\n"
                , FNAME);
    }
}

/* returns tm with monday of week 01 */
time_t getIsoWeekOne(int y) {
    int dateoff;
    time_t firstweek;
    struct tm *searchd;
    searchd = (struct tm *) malloc(sizeof(struct tm));

    searchd->tm_year  =  y; // current year
    searchd->tm_mon   =  0; // january
    searchd->tm_mday  =  4; // 4th of january
    searchd->tm_hour  = 12;
    searchd->tm_min   =  0;
    searchd->tm_sec   =  0; // noon that day
    searchd->tm_isdst = -1; // figure out if dst is in effect

    firstweek = mktime(searchd);
    if(firstweek == -1) {
        fprintf(stderr, "didn't work :(\n");
        exit(EXIT_FAILURE);
    } 

    dateoff = 1 - ((searchd->tm_wday) ? searchd->tm_wday : 7);
    //firstweek = firstweek + dateoff * SECDAYS;
    firstweek = firstweek + S_DAYS(dateoff);

    return firstweek;
}

Iwd *getIsoWeek(struct tm *param_s) {
    //struct tm *week1;
    time_t week1_ts;

    Iwd *iso_wd;
    iso_wd = (Iwd *) malloc(sizeof(Iwd));

    int year = param_s->tm_year;

    struct tm calib_curdate;
    struct tm calib_yearend;

    time_t calib_cd_ts;
    time_t calib_ye_ts;

    /* calibrated current date to noon */
    calib_curdate.tm_year  = year;
    calib_curdate.tm_mon   = param_s->tm_mon;
    calib_curdate.tm_mday  = param_s->tm_mday;
    calib_curdate.tm_hour  = 12;
    calib_curdate.tm_min   =  0;
    calib_curdate.tm_sec   =  0;
    calib_curdate.tm_isdst = -1;

    calib_yearend = calib_curdate;

    /* calibrated end of year to noon */
    calib_yearend.tm_mon   = 11;
    calib_yearend.tm_mday  = 29;

    calib_cd_ts = mktime(&calib_curdate);
    calib_ye_ts = mktime(&calib_yearend);

    if(calib_cd_ts >= calib_ye_ts) { /* param_s >= year, 12, 29 */
        week1_ts = getIsoWeekOne(year +1);

        if(calib_cd_ts < week1_ts) { /* param_s < week1_ts */
            week1_ts = getIsoWeekOne(year);
            printf("DEBUG: %d\n", param_s->tm_wday);
        } else {
            year++;
        }
    } else {
        week1_ts = getIsoWeekOne(year);

        if(calib_cd_ts < week1_ts) { /* param_s < week1_ts */
            week1_ts = getIsoWeekOne(--year);
        }
    }

    // at this point year should be correctly adjusted
    iso_wd->iw_year = year + 1900;
    iso_wd->iw_week = ((calib_cd_ts - week1_ts) / S_DAYS(1)) / 7 + 1;
    iso_wd->iw_wday = (param_s->tm_wday) ? param_s->tm_wday : 7;

    return iso_wd;
}

int main(int argc, char *argv[]) {
    /* extern long timezone; predefined by time.h */

    int opt;

    while((opt = getopt(argc, argv, optstring)) != -1) {
        switch(opt) {
            case 'h':
                usage(1);
                return EXIT_SUCCESS;
                break;
            case 'b':
                basic = 1;
                break;
            case 'd':
                sh_date = 1;
                break;
            case 't':
                sh_time = 1;
                break;
            case 'o':
                sh_offset = 1;
                break;
            case 'w':
                sh_date = 1;
                ordinal = 1;
                weekdate = 1;
                break;
            case 'l':
                sh_date = 1;
                ordinal = 1;
                break;
            case 'v':
                usage(0);
                return EXIT_SUCCESS;
                break;
            default:
                exit(EXIT_FAILURE);
                break;
        }
    }

    // no output at all? assume full ISO date timestamp
    if(!(sh_date || sh_time || sh_offset)) {
        sh_date = sh_time = sh_offset = 1;
    }

    int o_hh, o_mm;
    struct tm *time_s;
    time_t time_lc;
    char buffer[80] = {0};

    char date_f[80] = {0};
    char time_f[80] = {0};

    char sign = '+';

    time_lc = time(NULL);
    time_s = localtime(&time_lc);

#ifdef __DJGPP__
    int timezone = time_s->__tm_gmtoff;
#endif

    timezone = -timezone;
    if(timezone < 0) {
        sign = '-';
    }

    if(ordinal) {
        if(basic)
            sprintf(date_f, "%%Y%d", time_s->tm_yday +1);
        else
            sprintf(date_f, "%%Y-%d", time_s->tm_yday +1);

        if(weekdate) {
            Iwd *weekdate;
            weekdate = getIsoWeek(time_s);

            if(basic)
                sprintf(date_f, "%04dW%02d%d", weekdate->iw_year, weekdate->iw_week, weekdate->iw_wday);
            else
                sprintf(date_f, "%04d-W%02d-%d", weekdate->iw_year, weekdate->iw_week, weekdate->iw_wday);

        }

    } else {
        if(basic)
            sprintf(date_f, "%%Y%%m%%d");
        else
            sprintf(date_f, "%%Y-%%m-%%d");
    }

    if(basic)
        sprintf(time_f, "%%H%%M%%S");
    else
        sprintf(time_f, "%%H:%%M:%%S");

    // convert timezones to ISO 8601 offset
    o_hh = timezone / 3600;
    o_mm = (timezone - o_hh * 3600) / 60;

    if(sh_date) {
        strftime(buffer, 80, date_f, time_s);
        printf("%s", buffer);
    }

    if(sh_date && sh_time)
        printf("T");

    if(sh_time) {
        strftime(buffer, 80, time_f, time_s);
        printf("%s", buffer);
    }

    if(sh_offset) {
        printf("%c%02d", sign, o_hh);

        if(! basic)
            printf(":");

        printf("%02d", o_mm);
    }

    printf("\n"); 
    return EXIT_SUCCESS;
}
